/**
 * Класс StringProcessor с набором методов, реализующий некоторые действия со строчкам
 */
public class StringProcessor {

    /**
     * Метод возращает строку, состоящую из N копий строки s
     *
     * @param s - строка
     * @param n - целое число
     * @return Строка, состоящая из n копий строки s, записанных подряд;
     * При n = 0 результат — пустая строка;
     * При n < 0 выбрасывается исключение.
     */
    public static String stringRepeat(String s, int n) throws IllegalArgumentException {
        if (n > 0) {
            StringBuilder res = new StringBuilder();

            for (int i = 0; i < n; i++) {
                res.append(s);
            }
            return res.toString();
        }
        else if (n == 0) {
            return "";
        }
        else {
            throw new IllegalArgumentException("Negative value of repeat - impossible");
        }
    }

    /**
     * Метод находит количество вхождений второй строки в первую
     *
     * @param s  - строка
     * @param subs - подстрока
     * @return Количество вхождений второй строки в первую
     */
    public static int countOfEntry(String s, String subs) throws IllegalArgumentException{
        if (subs.length() > s.length()) {
            throw new IllegalArgumentException("Too long substring");
        } else if (s.equals(subs)) {
            return 1;
        } else {
            int counter = 0;
            int subsIndex = 0;
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == subs.charAt(subsIndex)) {
                    subsIndex++;
                    if (subsIndex == subs.length()) {
                        counter++;
                        subsIndex = 0;
                    }
                }
            }
            return counter;
        }
    }

    /**
     * Метод строит по строке новую строку, которая получена из исходной заменой каждого
     * символа '1' на подстроку ”один”, символа ‘2’ на подстроку “два” и символа ‘3’ на
     * подстроку “три”.
     *
     * @param s - исходная строка
     */
    public static String replace123(String s) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            int temp = s.charAt(i);
            if (temp == 49) {
                result.append("один");
            } else if (temp == 50) {
                result.append("два");
            } else if (temp == 51) {
                result.append("три");
            } else {
                result.append(s.charAt(i));
            }
        }
        return result.toString();
    }

    /**
     * Метод в строке типа StringBuilder удаляет каждый второй по счету символ.
     *
     * @param s - исходная изменяемая строка
     */
    public static StringBuilder deleteEven(StringBuilder s) {
        for (int i = 0; i < s.length(); i++) {
            s.delete(i, i + 1);
        }
        return s;
    }

    /**
     * Метод в строке типа StringBuilder меняет местами первое и последние слова.
     *
     * @param s  - исходная изменяемая строка
     */
    public static StringBuilder changeWords(StringBuilder s) throws IllegalArgumentException {
        int firstWordFirstCharIndex = -1;
        int firstWordFinalCharIndex = -1;
        int lastWordFirstCharIndex = -1;
        int lastWordFinalCharIndex = -1;
        boolean isCharBefore = false;
        boolean isFirst = true;

        for (int i = 0; i < s.length(); i++) {
            char temp = s.charAt(i);
            if (Character.isLetter(temp)) {
                if (!isCharBefore) {
                    isCharBefore = true;
                    if (isFirst) {
                        firstWordFirstCharIndex = i;
                    } else {
                        lastWordFirstCharIndex = i;
                    }
                }
            }
            else if(isCharBefore) {
                isCharBefore = false;
                if (isFirst) {
                    firstWordFinalCharIndex = i;
                    isFirst = false;
                } else {
                    lastWordFinalCharIndex = i;
                }
            }
        }

        if (isCharBefore && !isFirst) {
            lastWordFinalCharIndex = s.length();
        }

        String firstWord = s.substring(firstWordFirstCharIndex, firstWordFinalCharIndex);
        String lastWord = s.substring(lastWordFirstCharIndex, lastWordFinalCharIndex);
        s.replace(lastWordFirstCharIndex, lastWordFinalCharIndex, firstWord);
        s.replace(firstWordFirstCharIndex, firstWordFinalCharIndex, lastWord);
        return s;
    }

    /**
     * Метод  строит новую строку, в которой шестнадцатеричные числа будут
     * заменены на десятичные эквиваленты
     *
     * @param s - исходная строка
     */
    public static String numberViewChanger(String s) {
        int subsFirstIndex = s.indexOf("0x");
        if (subsFirstIndex == -1) {
            return s;
        } else {
            StringBuilder res = new StringBuilder();
            int i = 0;
            int subsLastIndex = 0;
            boolean isDigitBefore = true;
            while (i != s.length() - 1 && subsFirstIndex != -1) {
                res.append(s, i, subsFirstIndex);
                for (i = subsFirstIndex; i < s.length() && isDigitBefore; i++) {
                    char temp = s.charAt(i);
                    if (temp == ' ') {
                        isDigitBefore = false;
                        subsLastIndex = i;
                    }
                }

                if (isDigitBefore) {
                    res.append(Integer.decode(s.substring(subsFirstIndex)));
                    return res.toString();
                } else {
                    res.append(Integer.decode(s.substring(subsFirstIndex, subsLastIndex)));
                    String temp = s.substring(subsLastIndex);
                    if (temp.contains("0x")) {
                        isDigitBefore = true;
                        i--;
                        subsFirstIndex = i + temp.indexOf("0x");
                    } else {
                        subsFirstIndex = -1;
                    }
                }
            }

            if (isDigitBefore) {
                res.append(Integer.decode(s.substring(subsFirstIndex)));
            } else {
                res.append(s.substring(subsLastIndex));
            }
            return res.toString();

        }

    }

}