import java.util.ArrayList;
import java.util.Arrays;

public class FinanceReportProcessor{

    private static final String[] MONTHS = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август",
            "Сентябрь","Октябрь", "Ноябрь", "Декабрь"};

    /**
     * Получение платежей всех людей, чья фамилия начинается на указанный символ
     * @param financeReport - объект класса FinanceReport
     * @param fioBegin - символ с которого начинается фамилия
     * @return объект класса FinanceReport, cодержащий платежи всех людей, чья фамилия начинается на указанный символ
     */
    public static FinanceReport getPaymentsWithSelectedChar(FinanceReport financeReport, char fioBegin)
            throws IllegalArgumentException{
        ArrayList<Payment> arr = new ArrayList<>();
        int counter = 0;

        for(int i = 0; i < financeReport.getArr().length; i++){
            char checkedChar = financeReport.getArr()[i].getFio().charAt(0);
            if(fioBegin == checkedChar){
                arr.add(financeReport.getPayment(i));
                counter++;
            }
        }
        if(counter > 0) {
            return new FinanceReport(arr.toArray(Payment[]::new), financeReport.getOrgFio(), financeReport.getDate());
        }
        else{
            throw new IllegalArgumentException("Массив не содержит таких платежей");
        }
    }
    /**
     * Получение получение всех платежей, размер которых меньше заданной величины.
     * @param financeReport - объект класса FinanceReport
     * @param amount - размер максималного платежа,
     * @return объект класса FinanceReport, cодержащий платежи, размер которых меньше заданной величины.
     */
    public static FinanceReport getPaymentsWithSelectedAmount(FinanceReport financeReport, int amount)
            throws IllegalArgumentException{
        ArrayList<Payment> arr = new ArrayList<>();
        int counter = 0;
        int checkedAmount;
        for(int i = 0; i < financeReport.getArr().length; i++){
            checkedAmount = financeReport.getArr()[i].getAmount();
            if(checkedAmount < amount){
                arr.add(financeReport.getPayment(i));
                counter++;
            }
        }
        if(counter > 0) {
            return new FinanceReport(arr.toArray(Payment[]::new), financeReport.getOrgFio(), financeReport.getDate());
        }
        else{
            throw new IllegalArgumentException("Массив не содержит таких платежей");
        }
    }

    /**
     * Метод вычисляющий суммарный платеж на эту дату
     * @param financeReport - объект класса FinanceReport
     * @param date - строка с датой в формате dd.mm.yy
     * @return суммарный платеж наэту дату
     */
    public static int sumWithSelectedDate(FinanceReport financeReport, String date) throws IllegalArgumentException{
        int day = Integer.parseInt(date.substring(0, 2));
        int month = Integer.parseInt(date.substring(3,5));
        int year = Integer.parseInt(date.substring(6,8));
        int res = 0;
        int checkDay, checkYear, checkMonth;


        for(int i = 0; i < financeReport.getArr().length; i++){
            checkDay = financeReport.getArr()[i].getDay();
            if(checkDay == day){
                checkMonth = financeReport.getArr()[i].getMonth();
                if(checkMonth == month){
                    checkYear = financeReport.getArr()[i].getYear();
                    if(checkYear % 100 == year){
                        res += financeReport.getArr()[i].getAmount();
                    }
                }
            }
        }
        if(res > 0) {
            return res;
        }
        else{
            throw new IllegalArgumentException("Массив не содержит таких платежей");
        }
    }

    /**
     * Метод который определяет список названий месяцев, в которых не было ни
     * одного платежа в течение этого года.
     * @param fin - объект класса FinanceReport
     * @param year - Год(число)
     * @return Список названий месяцев, в которых не было ни одного платежа в течение этого года.
     */
    public static String[] getMonthsWithSelectedYear(FinanceReport fin, int year){
        ArrayList<String> months = new ArrayList<>(Arrays.asList(MONTHS));
        int checkYear, checkMonth;
        for(int i = 0; i < fin.getArr().length; i++){
            checkYear = fin.getArr()[i].getYear();
            if(checkYear == year){
                checkMonth = fin.getArr()[i].getMonth();
                months.remove(MONTHS[checkMonth-1]);
            }
        }
        return months.toArray(String[]::new);
    }
}

