import java.util.Objects;

/**
 * Класс Payment(платеж) с полями: ФИО человека (одна строка), дата
 * платежа — число, месяц и год (целые числа), сумма платежа (целое число — сумма
 * в копейках)
 */
public class Payment {

    private String fio;
    private int month, day, year;
    private int amount;

    /**
     * Конструктор по фио, дате и сумме
     */
    Payment(String fio, int month, int day, int year, int amount){
        this.fio = fio;
        this.month = month;
        this.day = day;
        this.year = year;
        this.amount = amount;
    }

    /**
     * Сеттеры и геттеры
     */
    public void setFio(String fio){
        this.fio = fio;
    }

    public void setMonth(int month){
        this.month = month;
    }

    public void setDay(int day){
        this.day = day;
    }

    public void setYear(int year){
        this.year = year;
    }

    public void setAmount(int amount){
        this.amount = amount;
    }

    public String getFio(){
        return fio;
    }

    public int getMonth(){
        return month;
    }

    public int getDay(){
        return day;
    }

    public int getYear(){
        return year;
    }

    public int getAmount(){
        return amount;
    }

    /**
     * Метод сравнивает два платежа
     * @param payment - сравниваемый платеж
     * @return равны ли платежи
     */
    public boolean equals(Payment payment) {
        if (this == payment) {
            return true;
        }
        if (payment == null || getClass() != payment.getClass()) {
            return false;
        }
        return month == payment.month && day == payment.day
                && year == payment.year && amount == payment.amount && payment.fio.equals(fio);
    }

    /**
     * Метод преобразует все данные платежа в одну строку
     * @return Строку, cодержащую данные о платеже
     */
    @Override
    public String toString(){
        return String.format("Плательщик: %s D/M/Y:%d/%d/%d Сумма: %d руб. %d коп.", fio, day, month, year,
                amount/100, amount % 100);
    }

    /**
     * Метод числовое значение фиксированной длины(hashcode) для платежа
     * @return hashcode платежа
     */
    @Override
    public int hashCode() {
        return Objects.hash(fio, month, day, year, amount);
    }

}