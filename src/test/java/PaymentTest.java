import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PaymentTest {

    @Test
    void testEqualsWithSamePayment() {
        Payment test1 = new Payment("А. С. Иванов", 1,4,1965,100);
        Payment test2 = new Payment("А. С. Иванов", 1,4,1965,100);
        assertTrue(test1.equals(test2));

    }

    @Test
    void testEqualsWithDifferentPayments() {
        Payment test = new Payment("А. С. Иванов", 1,4,1965,100);
        Payment test1 = new Payment("А. С. Сидоров", 1,2,1993,12200);
        assertFalse(test.equals(test1));
    }


    @Test
    void testToString() {
        Payment test = new Payment("А. С. Сидоров", 1,2,1993,12200);
        assertEquals("Плательщик: А. С. Сидоров D/M/Y:2/1/1993 Сумма: 122 руб. 0 коп.",test.toString());
    }
}