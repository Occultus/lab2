import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StringProcessorTest {

    @Test
    void testStringRepeatWithCorrectParameter() {
        String test = "Предложение для проверки";

        assertEquals("Предложение для проверкиПредложение для проверкиПредложение для проверки",
                StringProcessor.stringRepeat(test, 3));
    }

    @Test
    void testStringRepeatWithWrongParameter() {
        String test = "Предложение для проверки";

        assertThrows(IllegalArgumentException.class,()-> StringProcessor.stringRepeat(test, -1));
    }



    @Test
    void testCountOfEntryWithCorrectSubstring() {
        String test = "Предложение для для проверки";
        assertEquals(2,
                StringProcessor.countOfEntry(test, "для"));
    }

    @Test
    void testCountOfEntryWithVeryLongSubstring() {
        String test = "Предложение для для проверки";

        assertThrows(IllegalArgumentException.class,
                ()-> StringProcessor.countOfEntry(test,"Предложение для для проверки абвгд"));

    }

    @Test
    void testReplace123() {
        String test = "Предложение 1 для 3-ей проверки, а не для 2-ой";
        assertEquals("Предложение один для три-ей проверки, а не для два-ой",
                StringProcessor.replace123(test));
    }

    @Test
    void testDeleteEven() {
        StringBuilder test = new StringBuilder("Предложение");
        assertEquals("рдоеи", StringProcessor.deleteEven(test).toString());
    }

    @Test
    void testChangeWordsWithoutSpaces() {
        StringBuilder test = new StringBuilder("Слова даже разные");
        assertEquals("разные даже Слова", StringProcessor.changeWords(test).toString());
    }

    @Test
    void testChangeWordsWithSpaces() {
        StringBuilder test = new StringBuilder("    Слова даже разные    ");
        assertEquals("    разные даже Слова    ", StringProcessor.changeWords(test).toString());
    }

    @Test
    void numberViewChanger() {
        String test1 = "Проверка от 0xAB12 ";
        String test2 = "0xAB12 + 0xAC12 = 0x89";
        assertEquals("Проверка от 43794 ", StringProcessor.numberViewChanger(test1));
        assertEquals("43794 + 44050 = 137", StringProcessor.numberViewChanger(test2));
    }
}
