import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class FinanceReportProcessorTest {

    Payment[] payments = {
            new Payment("Ашаев И.В",7 ,12, 2019, 15230),
            new Payment("Ашаева Ю.М", 10,22, 2019, 8720),
            new Payment("Стругов Ю.Ф", 8,9, 2019, 632500),
            new Payment("Исаченко Н.А", 1,31, 2019, 400),
            new Payment("Ильев В.П", 4,14, 2019, 7301),
            new Payment("Толстуха А.С", 8,1, 2019, 386400),
            new Payment("Горбунов Н.В", 8,1, 2019, 42100),
            new Payment("Карелина Р.О", 8,1, 2019, 1475),
            new Payment("Кукина Е.Г", 11,19, 2019, 96333),
            new Payment("Салугин Ф.В", 6,2, 2019, 7200),
    };
    FinanceReport test = new FinanceReport(payments, "Латыпов И.А", new Date(1));


    @Test
    void testGetPaymentsWithSelectedChar() {
        assertEquals("[Автор:Латыпов И.А, Дата:Thu Jan 01 06:00:00 OMST 1970, Платежи:[\n" +
                "Плательщик: Ашаев И.В D/M/Y:12/7/2019 Сумма: 152 руб. 30 коп.\n" +
                "Плательщик: Ашаева Ю.М D/M/Y:22/10/2019 Сумма: 87 руб. 20 коп.\n" +
                "]]", FinanceReportProcessor.getPaymentsWithSelectedChar(test, 'А').toString());
    }

    @Test
    void testGetPaymentsWithSelectedAmount() {
        assertEquals("[Автор:Латыпов И.А, Дата:Thu Jan 01 06:00:00 OMST 1970, Платежи:[\n" +
                "Плательщик: Ашаев И.В D/M/Y:12/7/2019 Сумма: 152 руб. 30 коп.\n" +
                "Плательщик: Ашаева Ю.М D/M/Y:22/10/2019 Сумма: 87 руб. 20 коп.\n" +
                "Плательщик: Исаченко Н.А D/M/Y:31/1/2019 Сумма: 4 руб. 0 коп.\n" +
                "Плательщик: Ильев В.П D/M/Y:14/4/2019 Сумма: 73 руб. 1 коп.\n" +
                "Плательщик: Карелина Р.О D/M/Y:1/8/2019 Сумма: 14 руб. 75 коп.\n" +
                "Плательщик: Салугин Ф.В D/M/Y:2/6/2019 Сумма: 72 руб. 0 коп.\n" +
                "]]", FinanceReportProcessor.getPaymentsWithSelectedAmount(test, 16000).toString());
    }

    @Test
    void testSumWithSelectedDate(){
        assertEquals(429975, FinanceReportProcessor.sumWithSelectedDate(test, "01.08.19"));
    }

    @Test
    void testGetMonthsWithSelectedYear() {
        String[] result = FinanceReportProcessor.getMonthsWithSelectedYear(test, 2019);
        String[] expectedResult = new String[]{"Февраль", "Март", "Май", "Сентябрь", "Декабрь"};
        assertArrayEquals(expectedResult, result);
    }

    @Test
    void testGetPaymentWithSelectedCharWrongParameter(){
        assertThrows(IllegalArgumentException.class,
                () -> FinanceReportProcessor.getPaymentsWithSelectedChar(test, 'Ш'));
    }

    @Test
    void testGetPaymentsWithSelectedAmountWrongParameter(){
        assertThrows(IllegalArgumentException.class,
                () -> FinanceReportProcessor.getPaymentsWithSelectedAmount(test,300));
    }

    @Test
    void testSumWithSelectedDateWrongParameter(){
        assertThrows(IllegalArgumentException.class,
                () -> FinanceReportProcessor.sumWithSelectedDate(test, "01.02.2018"));
    }

    @Test
    void testSumWithSelectedYearWrongParameter(){

        assertThrows(IllegalArgumentException.class,
                () -> FinanceReportProcessor.getPaymentsWithSelectedChar(test, 'Ш'));
    }

}